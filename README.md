# Query Builder

This is similar to [spatie's](https://github.com/spatie/laravel-query-builder) query builder and is more powerful. Currently this package doesn't supports API parameters like spatie. It will be supported in the coming enhancements. 

Using this package, you will be able to
* Create fluent syntax for your queries.
* Create complex queries for querying your data.
* Can create custom filters.
* Can easily integrate with [laravel excel](https://github.com/Maatwebsite/Laravel-Excel). 

## Install

In your composer.json add this
```json
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/cinio/query-builder.git"
        },
        {
            "type": "vcs",
            "url": "git@gitlab.com:cinio/base.git"
        }
    ]
```
Then run this command
<pre>composer require cinio/query-builder</pre>

## Usage

Lets put this url /members?created_at=01-05-2019+to+28-06-2019&member_id=03451305&username=hy141319 as an example

Base from the query above, your class will be written as below

```php

use Modules\Base\Queries\QueryBuilder;

class UserQuery extends QueryBuilder
{
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'users',
        ],
        'username' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'username'
        ],
        'member_id' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'member_id'
        ]
    ];

    public function query()
    {
        return User::select('users.*')->orderBy('users.created_at', 'desc');
    }
}

```
>  If you have installed [laravel module](https://gitlab.com/cinio/laravel-modules), you can simply run "php artisan make:query" for creating query builder class. 

In your controller you can simply

```php
    protected $userQuery;

    public function __construct(UserQuery $userQuery)
    {
        $this->userQuery = $userQuery;
    }

    public function index(Request $request)
    {
        $users = $this->userQuery
            ->setParameters($request->all())
            ->paginate();

        return view('user::admin.management.member.index')->with(compact('users'));
    }
```

## Adhoc Queries
Sometimes you need to tap something in your main query. 

```php
public function beforeBuild()
{
   // Do extra process before building the query here
   $this->builder->where('is_active', 1);
}

```
```php
public function afterBuild()
{
   // Do extra process after building the query here
   $this->builder->where('is_hidden',0);
}

```

### Functions
Below are the list functions you can use for getting data.
* **first()** - Returns the first record.
* **get()** - Returns a collection of records.
* **paginate()** - Returns a collection of paginated records.
* **count()** - Returns the record count.

## Filters
For every data we show to the user, we always provide filters for filtering.

![Screenshot_2019-06-13_at_3.32.22_PM](/uploads/dfd20c4dcfd6a8e147e7118262811268/Screenshot_2019-06-13_at_3.32.22_PM.png)

To make your life easy, query builder have provided some builtin filters were you can easily use.

Html semantics
* date_range - By default, the supported format is 01-05-2019+to+28-06-2019
* date
* text
* checkbox
* radio
* select
* multi_select

Operator semantics 
* range - By default, the supported format is 100+200
* like
* boolean
* equal
* less_than_equal
* less_than
* greater_than_equal
* greater_than

### Custom Filters
```php
namespace Modules\User\Queries\Filters;

use Modules\Base\Queries\QueryFilter;

class SearchUserFilter extends QueryFilter
{
    public function filter($builder)
    {        
        $value  = $this->parameter->get($this->name);
        return $builder->where(function ($builder) use ($value) {
            $builder->where('users.member_id', 'like', '%' . $value . '%')
            ->orWhere('users.username', 'like', '%' . $value . '%');
        });
    }
}
```

>  If you have installed [laravel module](https://gitlab.com/cinio/laravel-modules), you can simply run "php artisan make:filter" for creating filter class. 


You can then use the filter by specifying its snake cased name.

```php
protected $filters = [
  'search' => [
     'filter' => 'search_user',
     'table'  => 'users',
     'namespace' => 'Modules\User\Queries\Filters'
   ],
];

```

## Exporting

Make sure you've installed [laravel excel](https://github.com/Maatwebsite/Laravel-Excel) in your project. 

```php

use Modules\Base\Queries\QueryBuilder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserQuery extends QueryBuilder implements WithMapping, WithHeadings
{
    protected $filters = [
        'created_at' => [
            'filter' => 'date_range',
            'table'  => 'users',
        ],
        'username' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'username'
        ],
        'member_id' => [
            'filter' => 'text',
            'table'  => 'users',
            'column' => 'member_id'
        ]
    ];

    public function query()
    {
        return User::select('users.*')->orderBy('users.created_at', 'desc');
    }

    public function map($user): array
    {
        return [
                $user->created_at,
                $user->member_id,
                $user->username,
                $user->email
        ];
    }

    public function headings(): array
    {
        return [
                __('a_user_management_member.registration date'),
                __('a_user_management_member.member id'),
                __('m_transaction_summary.username'),
                __('a_user_management_member.email')
        ];
    }
}
```

In your controller you can simply

```php 
protected $userQuery;

public function __construct(UserQuery $userQuery)
{
   $this->userQuery = $userQuery;
}

public function export(Request $request)
{
   $this->userQuery->setParameters($request->all());
   return Excel::download($this->userQuery, 'test.xlsx');
}

```
