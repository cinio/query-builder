<?php

namespace QueryBuilder\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Helpers\FileTypeDetector;

class ExportThoughEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $query;
    protected $filename;
    protected $recipient;
    protected $subject;

    public function __construct($query, $filename, $recipient = null, $subject = 'Export report')
    {
        $this->query     = $query;
        $this->filename  = $filename;
        $this->subject   = $subject;
        $this->recipient = $recipient;
    }

    public function handle()
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        $email         = $this->recipient ?  $this->recipient->email : config('blockchain.support_email');
        $pathinfo      = pathinfo($this->filename);
        $filenameWoExt = $pathinfo['filename'];
        $filenameExt   = $pathinfo['extension'];

        $maxRecords = config('qbuilder.export.rows', 20000);
        $pages      = max((int) ceil($this->query->count() / $maxRecords), 1);

        $files = [];
        for ($i = 1; $i <= $pages; $i++) {
            $filename   = $filenameWoExt . str_random(5) . '_' . $i . '.' . $filenameExt;
            $path       = config('filesystems.disks.local.root') . DIRECTORY_SEPARATOR . $filename;
            $writerType = FileTypeDetector::detectStrict($path);
            $parameters = array_merge($this->query->getParameters(), ['page' => $i, 'limit' => $maxRecords]);
            $this->query->setParameters($parameters)->paginate();
            if (Excel::store($this->query, $filename, 'local', $writerType)) {
                $files[] = $path;
            }
        }

        if ($files) {
            $body = $this->subject;
            Mail::raw($body, function ($message) use ($email, $files, $body) {
                $message->subject($body);
                $message->to($email);
                foreach ($files as $file) {
                    $message->attach($file);
                }
            });
        }

        array_map("unlink", $files);
    }
}
